<pre>
  ┌───────┐
  │       │
  │  a:o  │  acolono.com
  │       │
  └───────┘
</pre>

CONTENTS OF THIS FILE
---------------------

 * Version
 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

VERSION
-------
Version: 1.0.0-alpha1


INTRODUCTION
------------

Module implements ACONA intergration for Drupal.


REQUIREMENTS
------------

This module requires php version >= 7.1.0

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.


CONFIGURATION
-------------

  - ACONA API credentials.

MAINTAINERS
-----------

Current maintainers:
 * Nikolay Grachev (Granik) - https://www.drupal.org/u/granik / developed by
 * Christian Ziegler (Criz) - https://www.drupal.org/u/criz


by acolono GmbH
---------------

~~we build your websites~~
we build your business

hello@acolono.com

www.acolono.com
www.twitter.com/acolono
www.drupal.org/acolono-gmbh
