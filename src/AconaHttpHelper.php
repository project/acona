<?php

namespace Drupal\acona;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\GuzzleException;

/**
 * Acona HTTP Helper Service.
 */
class AconaHttpHelper implements AconaHttpHelperInterface {

  use StringTranslationTrait;

  /**
   * Guzzle http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * ACONA module helper service.
   *
   * @var \Drupal\acona\AconaHelperInterface
   */
  protected $helper;

  /**
   * AconaHttpHelper Class Constructor.
   *
   * @param \GuzzleHttp\Client $http_client
   *   Guzzle HTTP Client.
   * @param \Drupal\acona\AconaHelperInterface $helper
   *   ACONA module helper service.
   */
  public function __construct(HttpClient $http_client, AconaHelperInterface $helper) {
    $this->httpClient = $http_client;
    $this->helper = $helper;
  }

  /**
   * {@inheritDoc}
   */
  public function sendRequest(string $url, array $query, string $method = 'GET', array $headers = []) {
    $token = $this->helper->getApiAccessToken();
    if (!empty($token)) {
      $headers['Authorization'] = "Bearer {$token}";
    }

    // @todo check these settings.
    $options = [
      'http_errors' => TRUE,
      'force_ip_resolve' => 'v4',
      'connect_timeout' => 10,
      'read_timeout' => 10,
      'timeout' => 10,
      'verify' => FALSE,
      'headers' => $headers,
      'query' => $query,
    ];

    try {
      $res = $this->httpClient->request($method, $url, $options);
    }
    catch (GuzzleException $e) {
      $this->helper->log(
        $this->t('Cannot connect to the Acona data API. Message: @msg', [
          '@msg' => $e->getMessage(),
        ]),
        RfcLogLevel::WARNING
      );
      $res = FALSE;
    }

    $this->helper->log(
      $this->t('ACONA API request successful')
    );

    return $res;
  }

}
