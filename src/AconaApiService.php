<?php

namespace Drupal\acona;

use Drupal\Component\Serialization\Json;
use Psr\Http\Message\ResponseInterface;

/**
 * Provides the ACONA API Service.
 */
class AconaApiService extends AconaAbstractApiService {

  /**
   * {@inheritDoc}
   */
  public function getLatestSuccessScores($site_base_url = NULL) : array {
    $api_base_url = $this->helper->getApiBaseUrl();
    $endpoints = $this->getApiEndpoints();
    $url = $api_base_url . $endpoints['scores_latest'];

    if (empty($site_base_url)) {
      $site_base_url = $this->helper->getSiteUrl();
    }
    $query = [
      'domain' => $site_base_url,
    ];

    $res = $this->httpHelper->sendRequest($url, $query);

    $data_decoded = [];
    if ($res instanceof ResponseInterface) {
      $data = $res->getBody()->getContents();
      $data_decoded = Json::decode($data);
    }

    return $data_decoded;
  }

  /**
   * {@inheritDoc}
   */
  public function getLatestRecommendations(string $uri, array $indication = self::INDICATION_ALL, $site_base_url = NULL) : array {
    $api_base_url = $this->helper->getApiBaseUrl();
    if (empty($site_base_url)) {
      $site_base_url = $this->helper->getSiteUrl();
    }

    $page_url = $site_base_url . $uri;

    $endpoints = $this->getApiEndpoints();
    $url = $api_base_url . $endpoints['recommendations_latest'];

    $query = [
      'url' => $page_url,
      'indication' => implode(',', $indication),
    ];

    $res = $this->httpHelper->sendRequest($url, $query);
    $data_decoded = [];
    if ($res instanceof ResponseInterface) {
      $data = $res->getBody()->getContents();
      $data_decoded = Json::decode($data);
    }

    return $data_decoded;
  }

  /**
   * {@inheritDoc}
   */
  public function getNotificationsByUrl(string $uri, $site_base_url = NULL) : array {
    $api_base_url = $this->helper->getApiBaseUrl();
    if (empty($site_base_url)) {
      $site_base_url = $this->helper->getSiteUrl();
    }

    $page_url = $site_base_url . $uri;

    $endpoints = $this->getApiEndpoints();
    $url = $api_base_url . $endpoints['notifications'];

    $query = [
      'url' => $page_url,
      'langcode' => 'en',
    ];

    $res = $this->httpHelper->sendRequest($url, $query);
    $data_decoded = [];
    if ($res instanceof ResponseInterface) {
      $data = $res->getBody()->getContents();
      $data_decoded = Json::decode($data);
    }

    return $data_decoded;
  }

  /**
   * {@inheritDoc}
   */
  public function getCurrentSuccessScore(string $uri, $site_base_url = NULL): int {
    $data = $this->getSuccessScoreHistory($uri, $site_base_url);
    if (empty($data)) {
      return 0;
    }
    // Sort array by date.
    usort($data, function ($a, $b) {
      return (int) str_replace('-', '', $a['date']) > (int) str_replace('-', '', $b['date']);
    });

    $newest_row = array_pop($data);
    return (int) $newest_row['value'];

  }

  /**
   * {@inheritDoc}
   */
  public function getSuccessScoreHistory(string $uri, $site_base_url = NULL): array {
    $api_base_url = $this->helper->getApiBaseUrl();
    if (empty($site_base_url)) {
      $site_base_url = $this->helper->getSiteUrl();
    }

    $url = $site_base_url . $uri;

    $endpoints = $this->getApiEndpoints();
    $api_url = $api_base_url . $endpoints['success_scores'];

    $query = [
      'url' => $url,
      'from_date' => date('Y-m-d', strtotime(date('Y-m-d') . '-6 months')),
    ];

    $res = $this->httpHelper->sendRequest($api_url, $query);
    $data_decoded = [];

    if ($res instanceof ResponseInterface) {
      $data = $res->getBody()->getContents();
      $data_decoded = Json::decode($data);
    }

    return $data_decoded;
  }

}
