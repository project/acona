<?php

namespace Drupal\acona\Plugin\Block;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Block\BlockBase;
use Drupal\Core\Session\AccountInterface;

/**
 * Provides a ACONA URLs list' Block.
 *
 * @Block(
 *   id = "acona_urls_list",
 *   admin_label = @Translation("ACONA URLs list"),
 *   category = @Translation("ACONA"),
 * )
 */
class AconaUrlsListBlock extends BlockBase {

  /**
   * {@inheritDoc}
   */
  public function build() {

    $render_array = [
      '#theme' => 'container',
      '#children' => [
        [
          '#type' => 'markup',
          '#markup' => $this->t('ACONA URL success scores are not loaded.'),
        ],
      ],
      '#attributes' => ['class' => ['acona-urls-block', 'js-acona-urls-block']],
      '#attached' => [
        'library' => 'acona/urls_list_block',
      ],
    ];

    return [$render_array];
  }

  /**
   * {@inheritDoc}
   */
  public function blockAccess(AccountInterface $account) {
    if (!$account->hasPermission('access acona urls_list')) {
      return AccessResult::forbidden();
    }
    return AccessResult::allowed();
  }

}
