<?php

namespace Drupal\acona;

use Drupal\Core\Cache\CacheBackendInterface;

/**
 * Acona Cache Helper service.
 */
class AconaCacheHelper implements AconaCacheHelperInterface {

  /**
   * Cache max age. 43200 sec. = 12 h.
   */
  const CACHE_MAX_AGE = 43200;

  /**
   * Cache tags will be used by caching.
   */
  const CACHE_TAGS = ['config:acona.settings'];

  /**
   * Last success scores cache key.
   */
  const LAST_SUCCESS_SCORES_BLOCK_CACHE_KEY = 'acona:success_scores_block_latest';

  /**
   * Last success scores cache key.
   */
  const LAST_SUCCESS_SCORE_CACHE_KEY = 'acona:success_score_latest';

  /**
   * Last recommendations cache key.
   */
  const LAST_RECOMMENDATIONS_CACHE_KEY = 'acona:recommendations_latest';

  /**
   * Last notifications cache key.
   */
  const NOTIFICATIONS_BY_URL_CACHE_KEY = 'acona:notifications_by_url';

  /**
   * Score history cache key.
   */
  const SUCCESS_STORY_HISTORY_CACHE_KEY = 'acona:success_score_history';

  /**
   * Cache store service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $cache;

  /**
   * AconaCacheHelper class constructor.
   *
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache
   *   Cache service.
   */
  public function __construct(CacheBackendInterface $cache) {
    $this->cache = $cache;
  }

  /**
   * {@inheritDoc}
   */
  public function getCachedValue(string $key) {
    $value = $this->cache->get($key);
    if (isset($value->data)) {
      return $value->data;
    }
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function setCachedValue(string $key, $value, array $tags = []): void {
    $tags = array_merge(static::CACHE_TAGS, $tags);
    $expire = time() + static::CACHE_MAX_AGE;
    $this->cache->set($key, $value, $expire, $tags);
  }

  /**
   * {@inheritDoc}
   */
  public function clearCachedValue(string $key): void {
    $this->cache->delete($key);
  }

}
