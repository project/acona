<?php

namespace Drupal\acona;

use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Messenger\MessengerInterface;

/**
 * Acona Helper Service Interface.
 */
interface AconaHelperInterface {

  /**
   * Get client's site URL.
   *
   * @return string
   *   Client site URL.
   */
  public function getSiteUrl() : string;

  /**
   * Get API base URL.
   *
   * @return string
   *   ACONA API base URL.
   */
  public function getApiBaseUrl() : string;

  /**
   * Show drupal message.
   *
   * @param string $text
   *   Message text.
   * @param string $type
   *   Message type.
   */
  public function showMessage(string $text, $type = MessengerInterface::TYPE_STATUS) : void;

  /**
   * Log the message.
   *
   * @param string $message
   *   Message to log.
   * @param int $type
   *   Log message type (level).
   */
  public function log(string $message, $type = RfcLogLevel::INFO) : void;

  /**
   * Get API access token.
   *
   * @return string
   *   Token.
   */
  public function getApiAccessToken() : string;

  /**
   * Return array of red, green, yellow recommendations.
   *
   * @param array $recommendations
   *   Source recommendations array.
   */
  public static function processRecommendationsArray(array &$recommendations) : void;

}
