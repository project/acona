<?php

namespace Drupal\acona;


/**
 * Acona HTTP Helper Service Interface.
 */
interface AconaHttpHelperInterface {

  /**
   * Send a request to data store API.
   *
   * @param string $url
   *   Url to send request.
   * @param array $query
   *   Query params array.
   * @param string $method
   *   Request method.
   * @param array $headers
   *   Additional headers array.
   *
   * @return \Psr\Http\Message\ResponseInterface|false
   *   Http Response Object.
   */
  public function sendRequest(
        string $url,
        array $query,
        string $method = 'GET',
        array $headers = []
    );

}
