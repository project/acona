<?php

namespace Drupal\acona;

/**
 * Provides a base class for ACONA api services.
 */
abstract class AconaAbstractApiService implements AconaApiServiceInterface {

  /**
   * ACONA module helper service.
   *
   * @var \Drupal\acona\AconaHelperInterface
   */
  protected $helper;

  /**
   * ACONA http helper service.
   *
   * @var \Drupal\acona\AconaHttpHelperInterface
   */
  protected $httpHelper;

  /**
   * Acona Api service constructor.
   *
   * @param \Drupal\acona\AconaHelperInterface $helper
   *   ACONA module helper service.
   * @param \Drupal\acona\AconaHttpHelperInterface $httpHelper
   *   ACONA http helper service.
   */
  public function __construct(AconaHelperInterface $helper, AconaHttpHelperInterface $httpHelper) {
    $this->helper = $helper;
    $this->httpHelper = $httpHelper;
  }

  /**
   * {@inheritDoc}
   */
  public function checkConnection() : bool {
    $url = $this->helper->getApiBaseUrl();

    if (!$this->httpHelper->sendRequest($url, [])) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Get ACONA API Endpoints.
   *
   * @return string[]
   *   Array of endpoints ('readable_name' => '/path/to/endpoint').
   */
  protected function getApiEndpoints() {
    return [
      'scores_latest' => '/rpc/acona_urls_success',
      'recommendations_latest' => '/rpc/recommendations_last',
      'success_scores' => '/rpc/acona_success_scores',
      'notifications' => '/rpc/notifications'
    ];
  }

}
