<?php

namespace Drupal\acona;

/**
 * Acona Manager Interface.
 */
interface AconaApiServiceInterface {

  /**
   * Indication critical.
   */
  const INDICATION_CRITICAL = 'red';

  /**
   * Indication warning.
   */
  const INDICATION_WARNING = 'yellow';

  /**
   * Indication OK.
   */
  const INDICATION_OK = 'green';

  /**
   * All indications.
   */
  const INDICATION_ALL = [
    self::INDICATION_CRITICAL,
    self::INDICATION_WARNING,
    self::INDICATION_OK,
  ];

  /**
   * Get latest scores by domain name.
   *
   * @return array
   *   Parsed array of values.
   */
  public function getLatestSuccessScores() : array;

  /**
   * Get latest acona recommendations.
   *
   * @param string $uri
   *   Page URL to get recommendations for.
   * @param array $indication
   *   Indicators list (red, yellow, green)
   *
   * @return array
   *   Latest recommendations.
   */
  public function getLatestRecommendations(string $uri, array $indication = self::INDICATION_ALL) : array;

  /**
   * Get latest (current) success score for the page.
   *
   * @param string $uri
   *   Relative page URL.
   *
   * @return int
   *   Current success score.
   */
  public function getCurrentSuccessScore(string $uri) : int;

  /**
   * Get success score history for the page.
   *
   * @param string $uri
   *   Page URL (starts with a slash).
   *
   * @return array
   *   Success score history.
   */
  public function getSuccessScoreHistory(string $uri) : array;

  /**
   * Check ACONA API connection status.
   *
   * @return bool
   *   Connection status.
   */
  public function checkConnection() : bool;

  /**
   * Get notifications for a specific url.
   *
   * @param string $uri
   *   Page URL to get notifications for.
   *
   * @return array
   *   Notifications.
   */
  public function getNotificationsByUrl(string $uri) : array;

}
