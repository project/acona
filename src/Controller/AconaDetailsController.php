<?php

namespace Drupal\acona\Controller;

use Drupal\acona\AconaApiServiceInterface;
use Drupal\acona\AconaHelper;
use Drupal\acona\AconaHelperInterface;
use Drupal\Core\Render\RendererInterface;
use Drupal\node\Entity\Node;
use Drupal\path_alias\AliasManagerInterface;
use Drupal\taxonomy\Entity\Term;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Controller provides detailed analysis data for URL.
 */
class AconaDetailsController extends AconaControllerBase {

  /**
   * Alias manager service.
   *
   * @var \Drupal\path_alias\AliasManagerInterface
   */
  protected $aliasManager;

  /**
   * ACONA helper service.
   *
   * @var \Drupal\acona\AconaHelperInterface
   */
  protected $helper;

  /**
   * Acona Details Controller class constructor.
   *
   * @param \Drupal\acona\AconaApiServiceInterface $api_service
   *   ACONA API Service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Renderer service.
   * @param \Drupal\path_alias\AliasManagerInterface $alias_manager
   *   URL alias manager.
   * @param \Drupal\acona\AconaHelperInterface $helper
   *   ACONA helper service.
   */
  public function __construct(AconaApiServiceInterface $api_service, RendererInterface $renderer, AliasManagerInterface $alias_manager, AconaHelperInterface $helper) {
    parent::__construct($api_service, $renderer);
    $this->aliasManager = $alias_manager;
    $this->helper = $helper;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('acona.api_caching'),
      $container->get('renderer'),
      $container->get('path_alias.manager'),
      $container->get('acona.helper'),
    );
  }

  /**
   * {@inheritDoc}
   */
  public function index(Request $request) {
    $page = urldecode($request->get('page', '/'));
    return $this->detailsPage($page);
  }

  /**
   * {@inheritDoc}
   */
  public function title() {
    return $this->t('ACONA URL analysis');
  }

  /**
   * Acona details for node.
   *
   * @param \Drupal\node\Entity\Node $node
   *   Node entity.
   *
   * @return array
   *   Page render array.
   */
  public function detailsNode(Node $node) {
    $alias = $this->aliasManager
      ->getAliasByPath('/node/' . $node->id());

    return $this->detailsPage($alias);
  }

  /**
   * Acona details for taxonomy term.
   *
   * @param \Drupal\taxonomy\Entity\Term $taxonomy_term
   *   Taxonomy term entity.
   *
   * @return array
   *   Page render array.
   */
  public function detailsTerm(Term $taxonomy_term) {
    $alias = $this->aliasManager
      ->getAliasByPath('/taxonomy/term/' . $taxonomy_term->id());

    return $this->detailsPage($alias);
  }

  /**
   * Details page creator.
   *
   * @param string $page_path
   *   Page URI starting with a slash.
   *
   * @return array
   *   Page render array.
   */
  protected function detailsPage(string $page_path) {

    if ($page_path[0] !== '/') {
      throw new NotFoundHttpException();
    }

    $recommendations = $this->apiService->getLatestRecommendations($page_path);
    $score = $this->apiService->getCurrentSuccessScore($page_path);
    $notifications = $this->apiService->getNotificationsByUrl($page_path);

    AconaHelper::processRecommendationsArray($recommendations);

    $absolute_url = $this->helper->getSiteUrl() . $page_path;

    return [
      '#theme' => 'acona_details_page',
      '#page_url_absolute' => $absolute_url,
      '#page_uri' => $page_path,
      '#success_score' => $score,
      '#recommendations' => $recommendations,
      '#notifications' => $notifications,
      '#attached' => [
        'library' => ['acona/score-history-styles', 'acona/score-history-scripts'],
      ],
    ];
  }

}
