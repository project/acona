<?php

namespace Drupal\acona\Controller;

use Drupal\Core\Url;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Acona AJAX controller.
 */
final class AconaAjaxController extends AconaControllerBase {

  /**
   * {@inheritDoc}
   */
  public function index(Request $request) {
    $data = $this->apiService->getLatestSuccessScores();
    $li = [];
    foreach ($data as $row) {
      $url_parsed = parse_url($row['url']);
      $li[] = [
        '#theme' => 'acona_urls_list_element',
        '#data' => $row,
        '#details_url' => Url::fromRoute('acona.details', [
          'page' => urlencode($url_parsed['path']),
        ]),
        '#wrapper_attributes' => [
          'class' => [],
        ],
      ];
    }

    $build = [
      '#theme' => 'item_list',
      '#list_type' => 'ul',
      '#items' => $li,
      '#attributes' => [
        'class' => ['acona-urls-block__list'],
      ],
      '#wrapper_attributes' => [
        'class' => [],
      ],
    ];

    $html = $this->renderer->render($build);

    return new Response($html);
  }

  /**
   * Score history to build a chart.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   Current request.
   *
   * @return \Symfony\Component\HttpFoundation\JsonResponse
   *   Json serialized response.
   */
  public function scoreHistory(Request $request) {
    $page = urldecode($request->get('page', '/'));

    return new JsonResponse(
      $this->apiService->getSuccessScoreHistory($page)
    );
  }

}
