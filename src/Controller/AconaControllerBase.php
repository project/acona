<?php

namespace Drupal\acona\Controller;

use Drupal\acona\AconaApiServiceInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

abstract class AconaControllerBase extends ControllerBase {

  /**
   * Renderer service.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected $renderer;

  /**
   * ACONA API Helper Service.
   *
   * @var \Drupal\acona\AconaCacheHelperInterface
   */
  protected $apiService;

  /**
   * Acona ajax controller class constructor.
   *
   * @param \Drupal\acona\AconaApiServiceInterface $api_service
   *   Acona manager service.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   Renderer service.
   */
  public function __construct(AconaApiServiceInterface $api_service, RendererInterface $renderer) {
    $this->apiService = $api_service;
    $this->renderer = $renderer;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('acona.api_caching'),
      $container->get('renderer'),
    );
  }

}