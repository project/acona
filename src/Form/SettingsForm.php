<?php

namespace Drupal\acona\Form;

use Drupal\acona\AconaApiServiceInterface;
use Drupal\acona\AconaHelperInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Acona module config form.
 */
class SettingsForm extends ConfigFormBase implements ContainerInjectionInterface {

  /**
   * Acona manager service.
   *
   * @var \Drupal\acona\AconaApiServiceInterface
   */
  protected $apiService;

  /**
   * ACONA helper service.
   *
   * @var \Drupal\acona\AconaHelperInterface
   */
  protected $helper;

  /**
   * SettingsForm class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Drupal\acona\AconaApiServiceInterface $api_service
   *   ACONA Api Service.
   * @param \Drupal\acona\AconaHelperInterface $helper
   *   ACONA Helper Service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, AconaApiServiceInterface $api_service, AconaHelperInterface $helper) {
    parent::__construct($config_factory);
    $this->apiService = $api_service;
    $this->helper = $helper;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('acona.api_caching'),
      $container->get('acona.helper')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId() {
    return 'acona_settings_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config = $this->config('acona.settings');

    $form['site_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Site URL'),
      '#default_value' => $config->get('site_url'),
      '#description' => $this->t('Site base URL to get analysis data for.'),
      '#maxlength' => 255,
      '#autocomplete_route_name' => FALSE,
      '#size' => 60,
    ];

    $form['api_url'] = [
      '#type' => 'url',
      '#title' => $this->t('Data API URL'),
      '#default_value' => $config->get('api_url'),
      '#description' => $this->t('Data API base URL.'),
      '#maxlength' => 255,
      '#autocomplete_route_name' => FALSE,
      '#size' => 60,
    ];

    $form['token'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Access Token'),
      '#cols' => 10,
      '#rows' => 5,
      '#default_value' => $config->get('token'),
      '#description' => $this->t('Token to access the Acona data API.'),
    ];

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('acona.settings');
    $config->set('site_url', rtrim($form_state->getValue('site_url'), '/'));
    $config->set('api_url', rtrim($form_state->getValue('api_url'), '/'));
    $config->set('token', $form_state->getValue('token'));
    $config->save();

    if (!$this->apiService->checkConnection()) {
      $this->helper->showMessage(
        $this->t('Could not connect to ACONA data API. See recent log messages.'),
        MessengerInterface::TYPE_WARNING
      );
    }
    else {
      $this->helper->showMessage(
        $this->t('Successfully connected to ACONA data API.'),
      );
    }

    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritDoc}
   */
  protected function getEditableConfigNames() {
    return [
      'acona.settings',
    ];
  }

}
