<?php

namespace Drupal\acona;

/**
 * Provides the ACONA API service with caching feature.
 */
class AconaCachingApiService extends AconaApiService {

  /**
   * ACONA Caching API service.
   *
   * @var \Drupal\acona\AconaCacheHelperInterface
   */
  protected $cacheHelper;

  /**
   * ACONA API caching service class constructor.
   *
   * @param \Drupal\acona\AconaHelperInterface $helper
   *   ACONA module helper service.
   * @param \Drupal\acona\AconaHttpHelperInterface $httpHelper
   *   ACONA http helper service.
   * @param \Drupal\acona\AconaCacheHelperInterface $cache_helper
   *   ACONA Cache helper service.
   */
  public function __construct(AconaHelperInterface $helper, AconaHttpHelperInterface $httpHelper, AconaCacheHelperInterface $cache_helper) {
    parent::__construct($helper, $httpHelper);
    $this->cacheHelper = $cache_helper;
  }

  /**
   * {@inheritDoc}
   */
  public function getLatestSuccessScores($site_base_url = NULL): array {
    $cache_key = AconaCacheHelper::LAST_SUCCESS_SCORES_BLOCK_CACHE_KEY;
    $data_cached = $this->cacheHelper->getCachedValue($cache_key);

    if (isset($data_cached)) {
      return $data_cached;
    }

    $data = parent::getLatestSuccessScores($site_base_url);
    $this->cacheHelper->setCachedValue($cache_key, $data);

    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function getLatestRecommendations(string $uri, array $indication = self::INDICATION_ALL, $site_base_url = NULL): array {
    $cache_key = $this->computeCacheKey(AconaCacheHelper::LAST_RECOMMENDATIONS_CACHE_KEY, $uri, $site_base_url);
    $data_cached = $this->cacheHelper->getCachedValue($cache_key);

    if (isset($data_cached)) {
      return $data_cached;
    }

    $data = parent::getLatestRecommendations($uri, $indication, $site_base_url);
    $this->cacheHelper->setCachedValue($cache_key, $data);

    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function getNotificationsByUrl(string $uri, $site_base_url = NULL): array {
    $cache_key = $this->computeCacheKey(AconaCacheHelper::NOTIFICATIONS_BY_URL_CACHE_KEY, $uri, $site_base_url);
    $data_cached = $this->cacheHelper->getCachedValue($cache_key);

    if (isset($data_cached)) {
      return $data_cached;
    }

    $data = parent::getNotificationsByUrl($uri, $site_base_url);
    $this->cacheHelper->setCachedValue($cache_key, $data);

    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function getCurrentSuccessScore(string $uri, $site_base_url = NULL) : int {
    $cache_key = $this->computeCacheKey(AconaCacheHelper::LAST_SUCCESS_SCORE_CACHE_KEY, $uri, $site_base_url);
    $data_cached = $this->cacheHelper->getCachedValue($cache_key);

    if (isset($data_cached)) {
      return $data_cached;
    }

    $data = parent::getCurrentSuccessScore($uri, $site_base_url);
    $this->cacheHelper->setCachedValue($cache_key, $data);

    return $data;
  }

  /**
   * {@inheritDoc}
   */
  public function getSuccessScoreHistory(string $uri, $site_base_url = NULL): array {
    $cache_key = $this->computeCacheKey(AconaCacheHelper::SUCCESS_STORY_HISTORY_CACHE_KEY, $uri, $site_base_url);
    $data_cached = $this->cacheHelper->getCachedValue($cache_key);

    if (isset($data_cached)) {
      return $data_cached;
    }

    $data = parent::getSuccessScoreHistory($uri, $site_base_url);
    $this->cacheHelper->setCachedValue($cache_key, $data);

    return $data;
  }

  /**
   * Compute cache key.
   *
   * @param string $prefix
   *   Cacke key prefix.
   * @param string $page_path
   *   Path to a site page starting with a '/'.
   * @param string|null $base_url
   *   Site Base url.
   *
   * @return string
   *   Cache key.
   */
  protected function computeCacheKey(string $prefix, string $page_path, string $base_url = NULL) : string {
    if (!empty($base_url)) {
      $page_path = $base_url . $page_path;
      $key = str_replace(':', '', $page_path);
      $key = str_replace('/', '_', $key);
      return $prefix . ':url:' . $key;
    }
    $key_name = $page_path !== '/' ? str_replace('/', '_', $page_path)
      : '_main';
    return $prefix . ':page' . $key_name;
  }

}
