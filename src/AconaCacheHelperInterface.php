<?php

namespace Drupal\acona;

/**
 * Acona Cache Helper Interface.
 */
interface AconaCacheHelperInterface {

  /**
   * Get value from cache.
   *
   * @param string $key
   *   Cache key to get value by.
   *
   * @return mixed
   *   Cached value.
   */
  public function getCachedValue(string $key);

  /**
   * Add value to the cache.
   *
   * @param string $key
   *   Key to save value under.
   * @param mixed $value
   *   Value to save.
   */
  public function setCachedValue(string $key, $value) : void;

  /**
   * Clear cached value.
   *
   * @param string $key
   *   Cache key.
   */
  public function clearCachedValue(string $key): void;
}