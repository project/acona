<?php

namespace Drupal\acona;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Logger\RfcLogLevel;
use Drupal\Core\Messenger\MessengerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * ACONA module helper service.
 */
class AconaHelper implements AconaHelperInterface {

  /**
   * ACONA Module Config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * Messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /**
   * Symfony request stack service.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Logger interface.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * ACONA module helper class constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Config factory service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   Request stack service.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   Drupal messenger service.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, RequestStack $request_stack, MessengerInterface $messenger, LoggerInterface $logger) {
    $this->config = $config_factory->get('acona.settings');
    $this->requestStack = $request_stack;
    $this->messenger = $messenger;
    $this->logger = $logger;
  }

  /**
   * {@inheritDoc}
   */
  public function getSiteUrl() : string {
    $site_url_configured = $this->config->get('site_url');

    $site_url_real = $this->requestStack->getCurrentRequest()
      ->getSchemeAndHttpHost();

    if (empty($site_url_configured)) {
      return $site_url_real;
    }

    return $site_url_configured;
  }

  /**
   * {@inheritDoc}
   */
  public function showMessage(string $text, $type = MessengerInterface::TYPE_STATUS) : void {
    $this->messenger->addMessage($text, $type);
  }

  /**
   * {@inheritDoc}
   */
  public function getApiBaseUrl(): string {
    return $this->config->get('api_url');
  }

  /**
   * {@inheritDoc}
   */
  public function log(string $message, $type = RfcLogLevel::INFO): void {
    $this->logger->log($type, $message);
  }

  /**
   * {@inheritDoc}
   */
  public function getApiAccessToken() : string {
    return $this->config->get('token');
  }

  /**
   * {@inheritDoc}
   */
  public static function processRecommendationsArray(array &$recommendations) : void {
    $red = [];
    $yellow = [];
    $green = [];

    foreach ($recommendations as $row) {
      switch ($row['indication']) {
        case AconaApiServiceInterface::INDICATION_CRITICAL:
          $red[] = $row;
          break;

        case AconaApiServiceInterface::INDICATION_WARNING:
          $yellow[] = $row;
          break;

        case AconaApiServiceInterface::INDICATION_OK:
          $green[] = $row;
          break;

      }
    }

    $recommendations = [];
    if (!empty($red)) {
      $recommendations['red'] = $red;
    }

    if (!empty($yellow)) {
      $recommendations['yellow'] = $yellow;
    }

    if (!empty($green)) {
      $recommendations['green'] = $green;
    }
  }

}
