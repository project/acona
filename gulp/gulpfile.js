const {
  src, dest, task, parallel, watch, series
} = require('gulp');
const { rollup } = require('gulp-rollup-2');
const { nodeResolve } = require('@rollup/plugin-node-resolve');
const { babel } = require('@rollup/plugin-babel');
const commonjs = require('@rollup/plugin-commonjs');
const stripComments = require('gulp-strip-json-comments');
const removeEmptyLines = require('gulp-remove-empty-lines');
const sass = require("gulp-sass")(require('sass'));;
const prefix = require("gulp-autoprefixer");
// const debug = require('gulp-debug');
// const using = require('gulp-using');

const jsDist = '../js';
const styleDist = '../css'

const babelOptions = {
  babelHelpers: 'runtime',
  exclude: 'node_modules/**',
  presets: [['@babel/preset-env', {
    useBuiltIns: false,
    // corejs: 3,
    modules: 'auto',
  }]],
  plugins: [
    ['@babel/plugin-transform-runtime', {
      runtimeHelpers: true,
    }],
  ],
};

const nodeResolveOptions = {
  moduleDirectories: ['./node_modules'],
};

// Build JS.
function jsOne() {
  return src('./js/acona_url_list.es6.js')
    .pipe(rollup({
      output: {
        file: 'acona_url_list.js',
        name: 'acona_url_list',
        format: 'iife',
      },
      plugins: [babel(babelOptions), nodeResolve(nodeResolveOptions), commonjs()],
    }))
    .pipe(stripComments())
    .pipe(removeEmptyLines())
    // .pipe(debug({minimal: false}))
    // .pipe(using())
    .pipe(dest(jsDist));
}

function jsTwo() {
  return src('./js/acona_score_history.es6.js')
    .pipe(rollup({
      output: {
        file: 'acona_score_history.js',
        name: 'acona_score_history',
        format: 'iife',
      },
      plugins: [babel(babelOptions), nodeResolve(nodeResolveOptions), commonjs()],
    }))
    .pipe(stripComments())
    .pipe(removeEmptyLines())
    // .pipe(debug({minimal: false}))
    // .pipe(using())
    .pipe(dest(jsDist));
}

// Build styles.
function scss() {
  return src('./css/*.scss')
    .pipe(sass({
      errLogToConsole: false,
      onError: (err) => {
        console.log(err);
      }
    }))
    .pipe(stripComments())
    .pipe(prefix())
    // .pipe(debug({minimal: false}))
    // .pipe(using())
    .pipe(dest(styleDist));
}

task('jsOne', jsOne);
task('jsTwo', jsTwo);
task('js', parallel('jsTwo', 'jsOne'));
task('styles', scss);
task('default', series('js'));
task('watch', () => {
  watch('./js/acona_url_list.es6.js', parallel('js'));
  // watch(styleSrcPattern, parallel('styles'));
});
