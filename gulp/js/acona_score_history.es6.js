import ajax from 'simple-ajax-vanilla';
import {
  Chart,
  ArcElement,
  LineElement,
  BarElement,
  PointElement,
  BarController,
  BubbleController,
  DoughnutController,
  LineController,
  PieController,
  PolarAreaController,
  RadarController,
  ScatterController,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  RadialLinearScale,
  TimeScale,
  TimeSeriesScale,
  Decimation,
  Filler,
  Legend,
  Title,
  Tooltip,
  SubTitle,
} from 'chart.js';

Chart.register(
  ArcElement,
  LineElement,
  BarElement,
  PointElement,
  BarController,
  BubbleController,
  DoughnutController,
  LineController,
  PieController,
  PolarAreaController,
  RadarController,
  ScatterController,
  CategoryScale,
  LinearScale,
  LogarithmicScale,
  RadialLinearScale,
  TimeScale,
  TimeSeriesScale,
  Decimation,
  Filler,
  Legend,
  Title,
  Tooltip,
  SubTitle
);
((Drupal, ajax, chart) => {
  Drupal.behaviors.aconaScoreHistory = {
    chartData: [],
    container: null,
    // The main function.
    async attach(context) {
      this.container = context.querySelector('.acona');
      this.chartData.x = [];
      this.chartData.y = [];
      const data = await this.ajaxGetData();
      const dataParsed = JSON.parse(data.response);

      dataParsed.sort((a, b) => {
        return a.date.replace('-', '') < b.date.replace('-', '') ? -1 : 1;
      });
      for (let item of dataParsed) {
        this.chartData.push({
          x: item.date,
          y: item.value,
        });
      }
      // Initialize all.
      this.initChart();
      this.initScoreCounter();
    },
    // Get data from API.
    ajaxGetData() {
      const url = this.container.getAttribute('data-url');
      return ajax({
        url: '/acona/ajax/score-history',
        method: 'POST',
        data: {
          page: url,
        },
      });
    },
    // Init chart js.
    initChart() {
      const ctx = document.getElementById('acona-success-score-chart').getContext('2d');
      const config = {
        type: 'line',
        data: {
          datasets: [{
            label: '',
            data: this.chartData,
            fill: true,
            borderColor: '#ce5c5a',
            backgroundColor: '#ce5c5a',
            pointBackgroundColor: '#000000',
            tension: 0.1,
            lineTension: 0.3
          }]
        },
        options: {
          legend: false,
          responsive: true,
          maintainAspectRatio: false,
          plugins:{
            legend: {
              display: false
            },
          },
          scales: {
            y: {
              suggestedMin: 0,
              suggestedMax: 100
            },
            xAxes: [{
              type: 'time',
              time: {
                // min: this.chartData.shift().x,
                // max: this.chartData.pop().x,
                unit: 'day'
              }
            }]
          }
        }
      };
      return new chart(ctx, config);
    },
    // Init animated score counter.
    initScoreCounter() {
      // Get all the Meters
      const meters = document.querySelectorAll(
        "svg.acona-score .acona-score__tacho-meter"
      );
      let value;
      meters.forEach((path) => {
        // Get the length of the path.
        let length = path.getTotalLength();
        value = parseInt(
          path.parentNode.querySelector(".acona-score__value1").innerHTML
        );
        // Calculate the percentage of the total length.
        let to = length * ((100 - value) / 100);
        // Trigger Layout in Safari hack https://jakearchibald.com/2013/animated-line-drawing-svg/
        path.getBoundingClientRect();
        // Set the Offset.
        path.style.strokeDashoffset = Math.max(0, to);
      });

      const obj = document.querySelector(".acona-score__value1");
      this._animateValue(obj, 0, value, 1200);
    },
    // Animate score counter.
    _animateValue(obj, start, end, duration) {
      let startTimestamp = null;
      const step = (timestamp) => {
        if (!startTimestamp) {
          startTimestamp = timestamp
        }
        const progress = Math.min((timestamp - startTimestamp) / duration, 1);
        obj.innerHTML = Math.floor(progress * (end - start) + start);
        if (progress < 1) {
          window.requestAnimationFrame(step);
        }
      };
      window.requestAnimationFrame(step);
    }
  };
})(Drupal, ajax, Chart);
