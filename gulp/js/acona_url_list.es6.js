import ajax from 'simple-ajax-vanilla'
((Drupal, ajax) => {

  Drupal.behaviors.aconaUrlList = {
    attach(context, settings) {
      const lists = Array.from(context.querySelectorAll('.js-acona-urls-block'));
      lists.forEach(async list => {
        const res = await this.ajaxGetData();
        if (!res.hasError) {
          list.innerHTML = res.response;
        }
      });
    },

    ajaxGetData() {
      return ajax({
        url: '/acona/ajax/block',
        method: 'POST',
        data: {},
      });
    },
  };

})(Drupal, ajax);
