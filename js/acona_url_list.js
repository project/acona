(function () {
  'use strict';
  function asyncGeneratorStep(gen, resolve, reject, _next, _throw, key, arg) {
    try {
      var info = gen[key](arg);
      var value = info.value;
    } catch (error) {
      reject(error);
      return;
    }
    if (info.done) {
      resolve(value);
    } else {
      Promise.resolve(value).then(_next, _throw);
    }
  }
  function _asyncToGenerator(fn) {
    return function () {
      var self = this,
          args = arguments;
      return new Promise(function (resolve, reject) {
        var gen = fn.apply(self, args);
        function _next(value) {
          asyncGeneratorStep(gen, resolve, reject, _next, _throw, "next", value);
        }
        function _throw(err) {
          asyncGeneratorStep(gen, resolve, reject, _next, _throw, "throw", err);
        }
        _next(undefined);
      });
    };
  }
  var commonjsGlobal = typeof globalThis !== 'undefined' ? globalThis : typeof window !== 'undefined' ? window : typeof global !== 'undefined' ? global : typeof self !== 'undefined' ? self : {};
  var runtime = {exports: {}};
  (function (module) {
  var runtime = (function (exports) {
    var Op = Object.prototype;
    var hasOwn = Op.hasOwnProperty;
    var undefined$1;                                  
    var $Symbol = typeof Symbol === "function" ? Symbol : {};
    var iteratorSymbol = $Symbol.iterator || "@@iterator";
    var asyncIteratorSymbol = $Symbol.asyncIterator || "@@asyncIterator";
    var toStringTagSymbol = $Symbol.toStringTag || "@@toStringTag";
    function define(obj, key, value) {
      Object.defineProperty(obj, key, {
        value: value,
        enumerable: true,
        configurable: true,
        writable: true
      });
      return obj[key];
    }
    try {
      define({}, "");
    } catch (err) {
      define = function(obj, key, value) {
        return obj[key] = value;
      };
    }
    function wrap(innerFn, outerFn, self, tryLocsList) {
      var protoGenerator = outerFn && outerFn.prototype instanceof Generator ? outerFn : Generator;
      var generator = Object.create(protoGenerator.prototype);
      var context = new Context(tryLocsList || []);
      generator._invoke = makeInvokeMethod(innerFn, self, context);
      return generator;
    }
    exports.wrap = wrap;
    function tryCatch(fn, obj, arg) {
      try {
        return { type: "normal", arg: fn.call(obj, arg) };
      } catch (err) {
        return { type: "throw", arg: err };
      }
    }
    var GenStateSuspendedStart = "suspendedStart";
    var GenStateSuspendedYield = "suspendedYield";
    var GenStateExecuting = "executing";
    var GenStateCompleted = "completed";
    var ContinueSentinel = {};
    function Generator() {}
    function GeneratorFunction() {}
    function GeneratorFunctionPrototype() {}
    var IteratorPrototype = {};
    define(IteratorPrototype, iteratorSymbol, function () {
      return this;
    });
    var getProto = Object.getPrototypeOf;
    var NativeIteratorPrototype = getProto && getProto(getProto(values([])));
    if (NativeIteratorPrototype &&
        NativeIteratorPrototype !== Op &&
        hasOwn.call(NativeIteratorPrototype, iteratorSymbol)) {
      IteratorPrototype = NativeIteratorPrototype;
    }
    var Gp = GeneratorFunctionPrototype.prototype =
      Generator.prototype = Object.create(IteratorPrototype);
    GeneratorFunction.prototype = GeneratorFunctionPrototype;
    define(Gp, "constructor", GeneratorFunctionPrototype);
    define(GeneratorFunctionPrototype, "constructor", GeneratorFunction);
    GeneratorFunction.displayName = define(
      GeneratorFunctionPrototype,
      toStringTagSymbol,
      "GeneratorFunction"
    );
    function defineIteratorMethods(prototype) {
      ["next", "throw", "return"].forEach(function(method) {
        define(prototype, method, function(arg) {
          return this._invoke(method, arg);
        });
      });
    }
    exports.isGeneratorFunction = function(genFun) {
      var ctor = typeof genFun === "function" && genFun.constructor;
      return ctor
        ? ctor === GeneratorFunction ||
          (ctor.displayName || ctor.name) === "GeneratorFunction"
        : false;
    };
    exports.mark = function(genFun) {
      if (Object.setPrototypeOf) {
        Object.setPrototypeOf(genFun, GeneratorFunctionPrototype);
      } else {
        genFun.__proto__ = GeneratorFunctionPrototype;
        define(genFun, toStringTagSymbol, "GeneratorFunction");
      }
      genFun.prototype = Object.create(Gp);
      return genFun;
    };
    exports.awrap = function(arg) {
      return { __await: arg };
    };
    function AsyncIterator(generator, PromiseImpl) {
      function invoke(method, arg, resolve, reject) {
        var record = tryCatch(generator[method], generator, arg);
        if (record.type === "throw") {
          reject(record.arg);
        } else {
          var result = record.arg;
          var value = result.value;
          if (value &&
              typeof value === "object" &&
              hasOwn.call(value, "__await")) {
            return PromiseImpl.resolve(value.__await).then(function(value) {
              invoke("next", value, resolve, reject);
            }, function(err) {
              invoke("throw", err, resolve, reject);
            });
          }
          return PromiseImpl.resolve(value).then(function(unwrapped) {
            result.value = unwrapped;
            resolve(result);
          }, function(error) {
            return invoke("throw", error, resolve, reject);
          });
        }
      }
      var previousPromise;
      function enqueue(method, arg) {
        function callInvokeWithMethodAndArg() {
          return new PromiseImpl(function(resolve, reject) {
            invoke(method, arg, resolve, reject);
          });
        }
        return previousPromise =
          previousPromise ? previousPromise.then(
            callInvokeWithMethodAndArg,
            callInvokeWithMethodAndArg
          ) : callInvokeWithMethodAndArg();
      }
      this._invoke = enqueue;
    }
    defineIteratorMethods(AsyncIterator.prototype);
    define(AsyncIterator.prototype, asyncIteratorSymbol, function () {
      return this;
    });
    exports.AsyncIterator = AsyncIterator;
    exports.async = function(innerFn, outerFn, self, tryLocsList, PromiseImpl) {
      if (PromiseImpl === void 0) PromiseImpl = Promise;
      var iter = new AsyncIterator(
        wrap(innerFn, outerFn, self, tryLocsList),
        PromiseImpl
      );
      return exports.isGeneratorFunction(outerFn)
        ? iter                                                        
        : iter.next().then(function(result) {
            return result.done ? result.value : iter.next();
          });
    };
    function makeInvokeMethod(innerFn, self, context) {
      var state = GenStateSuspendedStart;
      return function invoke(method, arg) {
        if (state === GenStateExecuting) {
          throw new Error("Generator is already running");
        }
        if (state === GenStateCompleted) {
          if (method === "throw") {
            throw arg;
          }
          return doneResult();
        }
        context.method = method;
        context.arg = arg;
        while (true) {
          var delegate = context.delegate;
          if (delegate) {
            var delegateResult = maybeInvokeDelegate(delegate, context);
            if (delegateResult) {
              if (delegateResult === ContinueSentinel) continue;
              return delegateResult;
            }
          }
          if (context.method === "next") {
            context.sent = context._sent = context.arg;
          } else if (context.method === "throw") {
            if (state === GenStateSuspendedStart) {
              state = GenStateCompleted;
              throw context.arg;
            }
            context.dispatchException(context.arg);
          } else if (context.method === "return") {
            context.abrupt("return", context.arg);
          }
          state = GenStateExecuting;
          var record = tryCatch(innerFn, self, context);
          if (record.type === "normal") {
            state = context.done
              ? GenStateCompleted
              : GenStateSuspendedYield;
            if (record.arg === ContinueSentinel) {
              continue;
            }
            return {
              value: record.arg,
              done: context.done
            };
          } else if (record.type === "throw") {
            state = GenStateCompleted;
            context.method = "throw";
            context.arg = record.arg;
          }
        }
      };
    }
    function maybeInvokeDelegate(delegate, context) {
      var method = delegate.iterator[context.method];
      if (method === undefined$1) {
        context.delegate = null;
        if (context.method === "throw") {
          if (delegate.iterator["return"]) {
            context.method = "return";
            context.arg = undefined$1;
            maybeInvokeDelegate(delegate, context);
            if (context.method === "throw") {
              return ContinueSentinel;
            }
          }
          context.method = "throw";
          context.arg = new TypeError(
            "The iterator does not provide a 'throw' method");
        }
        return ContinueSentinel;
      }
      var record = tryCatch(method, delegate.iterator, context.arg);
      if (record.type === "throw") {
        context.method = "throw";
        context.arg = record.arg;
        context.delegate = null;
        return ContinueSentinel;
      }
      var info = record.arg;
      if (! info) {
        context.method = "throw";
        context.arg = new TypeError("iterator result is not an object");
        context.delegate = null;
        return ContinueSentinel;
      }
      if (info.done) {
        context[delegate.resultName] = info.value;
        context.next = delegate.nextLoc;
        if (context.method !== "return") {
          context.method = "next";
          context.arg = undefined$1;
        }
      } else {
        return info;
      }
      context.delegate = null;
      return ContinueSentinel;
    }
    defineIteratorMethods(Gp);
    define(Gp, toStringTagSymbol, "Generator");
    define(Gp, iteratorSymbol, function() {
      return this;
    });
    define(Gp, "toString", function() {
      return "[object Generator]";
    });
    function pushTryEntry(locs) {
      var entry = { tryLoc: locs[0] };
      if (1 in locs) {
        entry.catchLoc = locs[1];
      }
      if (2 in locs) {
        entry.finallyLoc = locs[2];
        entry.afterLoc = locs[3];
      }
      this.tryEntries.push(entry);
    }
    function resetTryEntry(entry) {
      var record = entry.completion || {};
      record.type = "normal";
      delete record.arg;
      entry.completion = record;
    }
    function Context(tryLocsList) {
      this.tryEntries = [{ tryLoc: "root" }];
      tryLocsList.forEach(pushTryEntry, this);
      this.reset(true);
    }
    exports.keys = function(object) {
      var keys = [];
      for (var key in object) {
        keys.push(key);
      }
      keys.reverse();
      return function next() {
        while (keys.length) {
          var key = keys.pop();
          if (key in object) {
            next.value = key;
            next.done = false;
            return next;
          }
        }
        next.done = true;
        return next;
      };
    };
    function values(iterable) {
      if (iterable) {
        var iteratorMethod = iterable[iteratorSymbol];
        if (iteratorMethod) {
          return iteratorMethod.call(iterable);
        }
        if (typeof iterable.next === "function") {
          return iterable;
        }
        if (!isNaN(iterable.length)) {
          var i = -1, next = function next() {
            while (++i < iterable.length) {
              if (hasOwn.call(iterable, i)) {
                next.value = iterable[i];
                next.done = false;
                return next;
              }
            }
            next.value = undefined$1;
            next.done = true;
            return next;
          };
          return next.next = next;
        }
      }
      return { next: doneResult };
    }
    exports.values = values;
    function doneResult() {
      return { value: undefined$1, done: true };
    }
    Context.prototype = {
      constructor: Context,
      reset: function(skipTempReset) {
        this.prev = 0;
        this.next = 0;
        this.sent = this._sent = undefined$1;
        this.done = false;
        this.delegate = null;
        this.method = "next";
        this.arg = undefined$1;
        this.tryEntries.forEach(resetTryEntry);
        if (!skipTempReset) {
          for (var name in this) {
            if (name.charAt(0) === "t" &&
                hasOwn.call(this, name) &&
                !isNaN(+name.slice(1))) {
              this[name] = undefined$1;
            }
          }
        }
      },
      stop: function() {
        this.done = true;
        var rootEntry = this.tryEntries[0];
        var rootRecord = rootEntry.completion;
        if (rootRecord.type === "throw") {
          throw rootRecord.arg;
        }
        return this.rval;
      },
      dispatchException: function(exception) {
        if (this.done) {
          throw exception;
        }
        var context = this;
        function handle(loc, caught) {
          record.type = "throw";
          record.arg = exception;
          context.next = loc;
          if (caught) {
            context.method = "next";
            context.arg = undefined$1;
          }
          return !! caught;
        }
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];
          var record = entry.completion;
          if (entry.tryLoc === "root") {
            return handle("end");
          }
          if (entry.tryLoc <= this.prev) {
            var hasCatch = hasOwn.call(entry, "catchLoc");
            var hasFinally = hasOwn.call(entry, "finallyLoc");
            if (hasCatch && hasFinally) {
              if (this.prev < entry.catchLoc) {
                return handle(entry.catchLoc, true);
              } else if (this.prev < entry.finallyLoc) {
                return handle(entry.finallyLoc);
              }
            } else if (hasCatch) {
              if (this.prev < entry.catchLoc) {
                return handle(entry.catchLoc, true);
              }
            } else if (hasFinally) {
              if (this.prev < entry.finallyLoc) {
                return handle(entry.finallyLoc);
              }
            } else {
              throw new Error("try statement without catch or finally");
            }
          }
        }
      },
      abrupt: function(type, arg) {
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];
          if (entry.tryLoc <= this.prev &&
              hasOwn.call(entry, "finallyLoc") &&
              this.prev < entry.finallyLoc) {
            var finallyEntry = entry;
            break;
          }
        }
        if (finallyEntry &&
            (type === "break" ||
             type === "continue") &&
            finallyEntry.tryLoc <= arg &&
            arg <= finallyEntry.finallyLoc) {
          finallyEntry = null;
        }
        var record = finallyEntry ? finallyEntry.completion : {};
        record.type = type;
        record.arg = arg;
        if (finallyEntry) {
          this.method = "next";
          this.next = finallyEntry.finallyLoc;
          return ContinueSentinel;
        }
        return this.complete(record);
      },
      complete: function(record, afterLoc) {
        if (record.type === "throw") {
          throw record.arg;
        }
        if (record.type === "break" ||
            record.type === "continue") {
          this.next = record.arg;
        } else if (record.type === "return") {
          this.rval = this.arg = record.arg;
          this.method = "return";
          this.next = "end";
        } else if (record.type === "normal" && afterLoc) {
          this.next = afterLoc;
        }
        return ContinueSentinel;
      },
      finish: function(finallyLoc) {
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];
          if (entry.finallyLoc === finallyLoc) {
            this.complete(entry.completion, entry.afterLoc);
            resetTryEntry(entry);
            return ContinueSentinel;
          }
        }
      },
      "catch": function(tryLoc) {
        for (var i = this.tryEntries.length - 1; i >= 0; --i) {
          var entry = this.tryEntries[i];
          if (entry.tryLoc === tryLoc) {
            var record = entry.completion;
            if (record.type === "throw") {
              var thrown = record.arg;
              resetTryEntry(entry);
            }
            return thrown;
          }
        }
        throw new Error("illegal catch attempt");
      },
      delegateYield: function(iterable, resultName, nextLoc) {
        this.delegate = {
          iterator: values(iterable),
          resultName: resultName,
          nextLoc: nextLoc
        };
        if (this.method === "next") {
          this.arg = undefined$1;
        }
        return ContinueSentinel;
      }
    };
    return exports;
  }(
    module.exports 
  ));
  try {
    regeneratorRuntime = runtime;
  } catch (accidentalStrictMode) {
    if (typeof globalThis === "object") {
      globalThis.regeneratorRuntime = runtime;
    } else {
      Function("r", "regeneratorRuntime = r")(runtime);
    }
  }
  }(runtime));
  var regenerator = runtime.exports;
  var simpleAjax = {exports: {}};
  (function (module) {
  class AjaxResult {      constructor(req) {          if (!req || req.readyState !== 4)              throw new Error("Request has no result");          this.request = req;          this.response = req.response;          this.url = req.responseURL;          this.status = req.status;          this.statusText = req.statusText;          this.hasError = !(req.status >= 100 && req.status < 400);      }      get document() {          return this._get("document", () => {              let doc = this.request.responseXML;              if (!doc) {                  let contentType = this.headers["content-type"];                  if (contentType && (contentType.includes("xml") || contentType.includes("html")))                      doc = new DOMParser().parseFromString(this.text, contentType.split(';')[0]);              }              return doc;          });      }      get text() {          return this._get("text", () => this.request.responseText || (this.response || "").toString());      }      get headers() {          return this._get("headers", () => {              let rawHeaders = this.request.getAllResponseHeaders();              let result = (rawHeaders || "")              .split('\r\n')              .map(line => {                  let parts = line.split(': ');                  return parts.length > 1 ? [parts.shift(), parts.join(': ')] : null;              })              .filter(x => x)              .reduce((acc, val) => { acc[val[0]] = val[1]; return acc; }, {});              result.contains = function (name) {                  return this.hasOwnProperty(name.toLowerCase());              };              result.get = function(name) {                  return this[name.toLowerCase()];              };              return result;          });      }      get type() {          return this._get("type", () => {               let type = this.request.responseType;              if (!type) {                  type = this.headers["content-type"];                  if (type) {                      let parts = type.split('/');                      type = parts[parts.length - 1].split(';')[0];                  }              }              return type;          });      }      get value() {          return this._get("value", () => JSON.parse(this.text));      }      _get(name, init) {          let cacheName = `_${name}`;          if (this[cacheName] === undefined) {              try {                  this[cacheName] = init();              } catch (e) {                  this[cacheName] = null;              }          }          return this[cacheName];      }  }  const ajax = (function () {      const Request = XMLHttpRequest;      if (!Request)          throw new Error("XMLHttpRequest is unavailable");      const defaultSettings = {          url: "",          type: "GET",          content: null,          success: null,          error: null,          statusCode: null,          headers: null,          interval: 1000,          confirmation: () => true,          beforeSend: null,          beforeReturn: null,          modifier: null      };      function serializeContent(content) {          if (!content)              return new FormData();          if (content instanceof HTMLFormElement)              return new FormData(content);          if (typeof content === "string") {              let selectedValue = document.querySelector(content);              if (selectedValue instanceof HTMLFormElement)                  return new FormData(selectedValue);          }          let data = new FormData();          if (typeof content === "object") {              for (let key in content)                  data.append(key, content[key]);          } else {              data.append("value", content);          }          return data;      }      function formDataToUrl(formData) {          let url = "";          for (let key of formData.keys()) {              let encodedKey = encodeURIComponent(key);              let values = formData.getAll(key);              if (values.length === 1)                  url += `&${encodedKey}=${encodeURIComponent(values[0])}`;              else                  for (let i = 0; i < values.length; ++i)                      url += `&${encodedKey}[${i}]=${encodeURIComponent(values[i])}`;          }          return url.substring(1);      }      function installHeaders(request, headers) {          if (headers && typeof headers === "object")               for (let key in headers)                  request.setRequestHeader(key, headers[key]);      }      function createRequest(url, formData, type, headers, modifier) {          return new Promise(function (resolve) {              type = type.toUpperCase();              let req = new Request();              if (typeof modifier.onProgress === "function")                  req.addEventListener("progress", modifier.onProgress);              if (req.upload && typeof modifier.onUploadProgress === "function")                  req.upload.addEventListener("progress", modifier.onUploadProgress);              req.addEventListener("readystatechange", function () {                  if (req.readyState === 4) {                      let result = new AjaxResult(req);                      if (typeof modifier.beforeReturn === "function")                          modifier.beforeReturn(result, req);                      resolve(result);                  }              });              if (type === "GET" || type === "HEAD") {                  let query = formDataToUrl(formData);                  req.open(type, url + (query ? "?" : "") + query);                  installHeaders(req, headers);                  if (typeof modifier.beforeSend === "function")                      modifier.beforeSend(req);                  req.send();              } else {                  req.open(type, url);                  installHeaders(req, headers);                  if (typeof modifier.beforeSend === "function")                      modifier.beforeSend(req);                  req.send(formData);              }          });      }      function createOptions(args) {          if (typeof args[0] === "string")              return createOptionsFromString(args[0], args[1]);          else if (args[0] instanceof HTMLFormElement)              return createOptionsFromForm(args[0], args[1]);          return args[0] || {};      }      function createOptionsFromString(url, options) {          return {              url: url,              ...(options || {})          };      }      function createOptionsFromForm(form, options) {          return {              url: form.action,              method: form.method,              data: form,              ...(options || {})          };      }      const ajax = function () {          let {              url,              method,              type,              data,              content,              success,              error,              statusCode,              headers,              onProgress,              onUploadProgress,              beforeSend,              beforeReturn,              modifier              } = {               ...ajax.defaultSettings,              ...createOptions([...arguments])          };          if (!url)              throw new Error("URL wasn't specified");          modifier = modifier || {};          modifier.onProgress = modifier.onProgress || onProgress;          modifier.onUploadProgress = modifier.onUploadProgress || onUploadProgress;          modifier.beforeSend = modifier.beforeSend || beforeSend;          modifier.beforeReturn = modifier.beforeReturn || beforeReturn;          let formData = serializeContent(data || content);          let request = createRequest(url, formData, method || type || "GET", headers, modifier);          if (statusCode)              request = request.then(response => {                  let func = statusCode[response.status];                  if (func)                      func(response);                  return response;              });          if (success)              request = request.then(response => { if (!response.hasError) success(response); return response; });          if (error)              request = request.then(response => { if (response.hasError) error(response); return response; });          return request;      };      ajax.defaultSettings = defaultSettings;      return ajax;  })();  HTMLFormElement.prototype.addAjax = function(handler, options) {      options = options || {};      let confirmation = options.confirmation;      confirmation = typeof confirmation === "function" ? confirmation : ajax.defaultSettings.confirmation;      let intervalMS = Number(options.interval);      intervalMS = isNaN(intervalMS) || intervalMS < 0 ? ajax.defaultSettings.interval : intervalMS;      let locked = false;      let eventHandler;      let reload = () => {          if (locked)              locked = false;          else              this.addEventListener("submit", eventHandler);      };      eventHandler = function (e) {          Promise.resolve(confirmation.bind(this)()).then(confirmed => {              if (confirmed !== false) {                  this.removeEventListener("submit", eventHandler);                  locked = true;                  setTimeout(reload, intervalMS);                  ajax(this, options).then(response => {                      reload();                      e.response = response;                      handler.bind(this)(e);                  });              }          });      };      this.addEventListener("submit", e => e.preventDefault());      this.addEventListener("submit", eventHandler);  };  (function (root, factory) {      {          module.exports = factory();      }  }(commonjsGlobal, function () {      return ajax;  }));
  }(simpleAjax));
  var ajax = simpleAjax.exports;
  (function (Drupal, ajax) {
    Drupal.behaviors.aconaUrlList = {
      attach: function attach(context, settings) {
        var _this = this;
        var lists = Array.from(context.querySelectorAll('.js-acona-urls-block'));
        lists.forEach(              function () {
          var _ref = _asyncToGenerator(              regenerator.mark(function _callee(list) {
            var res;
            return regenerator.wrap(function _callee$(_context) {
              while (1) {
                switch (_context.prev = _context.next) {
                  case 0:
                    _context.next = 2;
                    return _this.ajaxGetData();
                  case 2:
                    res = _context.sent;
                    if (!res.hasError) {
                      list.innerHTML = res.response;
                    }
                  case 4:
                  case "end":
                    return _context.stop();
                }
              }
            }, _callee);
          }));
          return function (_x) {
            return _ref.apply(this, arguments);
          };
        }());
      },
      ajaxGetData: function ajaxGetData() {
        return ajax({
          url: '/acona/ajax/block',
          method: 'POST',
          data: {}
        });
      }
    };
  })(Drupal, ajax);
})();
